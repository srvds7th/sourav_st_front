import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';
/*
axios.defaults.baseURL = 'http://127.0.0.1:8000/api';*/
/*axios.defaults.baseURL = 'http://127.0.0.1:8000';*/
axios.defaults.headers.common['X-Auth-Token'] = window.localStorage.getItem('X-Auth-Token');


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
