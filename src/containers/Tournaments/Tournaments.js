import React, {Component} from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
import {Link} from 'react-router-dom';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button'
import Aux from '../../hoc/Aux';
import classes from './Tournaments.css'


class Tournaments extends Component {
    state = {
        addTournamentName: '',
        tournaments: null,
        loading: false,
        error: false,
        input: "",
        update: false,
        tournamentStatus: {
            1: "Not started",
            2: "Started",
            3: "Completed"
        }
    }
    loadComponent () {
        
        this.setState({loading: true});
        axios.get( '/api/tournament/').then( response => {
                const tournaments = response.data
                this.setState( { tournaments: tournaments['tournaments'] } )
                this.setState({loading: false, error: false});
            }).catch( error => {
                console.log( error );
                this.setState({error: true, loading: false});
            } );
    }

    componentDidMount () {
        this.loadComponent();
    }

    addTournamentHandler=()=>{
        axios.post('/api/tournament/',{tournament: this.state.input}).then( response => {
            let newTournamentState = {...this.state.tournaments}
            let id=response.data.id;
            newTournamentState[id]=[response.data.name,1]
            this.setState({tournaments: newTournamentState, input: ""});
        }).catch(error => {
            console.log(error);
        })
    }
    changeHandler =(event)=> {
        this.setState({input: event.target.value})
    }
    render() {
        let tournaments = <p>Sorry something went wrong!</p>
        if (!this.state.error){
            tournaments = <p>Please wait... :)</p>
            if(this.state.loading){
                tournaments = <Spinner/>
            }
            else if (this.state.tournaments) {
                let rows = [];
                for (let i in this.state.tournaments) {
                    if(this.state.tournaments[i][1] === 1){
                        rows.push(
                            <Link key={i+'a'}
                                 className={classes.a} to={this.props.match.url+'/'+i}>
                                <li key ={i+'b'} className={classes.li}>{this.state.tournaments[i][0]}</li>
                                <span key={i+'c'} className={classes.span}>{this.state.tournamentStatus[this.state.tournaments[i][1]]}</span>
                            </Link>
                        );
                    }
                    else {
                        rows.push(
                            <Link key={i+'a'}
                                 className={classes.a} to={this.props.match.url+'/'+i+'/rounds'}>
                                <li key ={i+'b'} className={classes.li}>{this.state.tournaments[i][0]}</li>
                                <span key={i+'c'} className={classes.span}>{this.state.tournamentStatus[this.state.tournaments[i][1]]}</span>
                            </Link>
                        );
                    }
                }
                tournaments = rows;
            }
        }
        const input = {type: 'text', placeholder: 'Enter tournament name' }
        return(
            <Aux>
                <Input id="TournamentInput" value={this.state.input} label='Add tournament' elementConfig={input} changed={this.changeHandler}></Input>
                <Button btnType="Success" clicked={this.addTournamentHandler}> Add </Button>
                <div>
                <ul className={classes.ul}> Tournaments:
                    {tournaments}
                </ul>
                </div>
            </Aux>
        );
    }
}


export default Tournaments;