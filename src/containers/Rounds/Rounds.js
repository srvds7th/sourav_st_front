import React, {Component} from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
/*import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button'*/
import Aux from '../../hoc/Aux';
import PlayersList from '../../components/PlayersList/PlayersList'
import PlayerStanding from '../../components/PlayerStanding/PlayerStanding'
import RoundButtons from '../../components/RoundButtons/RoundButtons'
import Round from './Round/Round'
import Modal from '../../components/UI/Modal/Modal'
/*import classes from './Tournaments.css'*/


class Rounds extends Component {
    state = {
        players: null,
        scores: null,
        rounds: null,
        currentRound: null,
        swissPairs: null,
        tournamentStatus: null,
        roundPlayerStanding: null,
        loading: false,
        loadingModal: false,
        showStanding: false,
        showSwissPairs: false,
        error: false,
        playing: false,
    }
    loadComponent () {
        
        this.setState({loading: true});
        axios.get( '/api/tournament/'+ this.props.match.params.id).then( response => {
                const allPlayers = response.data
                this.setState( { players: allPlayers['player'], tournamentStatus: allPlayers['tournament_status']} )
                this.setState({loading: false, error: false});
            }).catch( error => {
                console.log( error );
                this.setState({loading: false, error: true});
            } );
        this.setState({loading: true});
        axios.get( '/api/tournament/'+ this.props.match.params.id+'/standings/').then( response => {
                const scores = response.data.score;
                const rounds = response.data.rounds;
                const currentRound = response.data.current_round;
                this.setState( { scores: scores, rounds: rounds, currentRound: currentRound } )
                this.setState({loading: false, error: false});
            }).catch( error => {
                console.log( error );
                this.setState({loading: false, error: true});
            } );
    }
    componentDidMount () {
        this.loadComponent();
    }
    componentDidUpdate() {

    }
    roundCancelHandler = () => {
      this.setState({playing: false, showStanding: false, showSwissPairs: false});
      /*this.loadComponent();*/
      this.forceUpdate();

    }
    startRoundHandler (currentRound) {
        console.log("This is startRoundHandler")
      this.setState({swissPairs: null, playing: true, loadingModal: true, showSwissPairs: true})
      axios.get('/api/tournament/'+this.props.match.params.id+'/status/'+currentRound+'/').then(response => {
          let swissPairs = response.data.key
          this.setState({swissPairs: swissPairs, loadingModal: false})
      }).catch(error => {
          console.log(error);
      })
    }
    showResult (roundNo) {
        this.setState({roundPlayerStanding: null, playing: true, showStanding: true, loadingModal: true})
        let data = {}
        data['tournament_id']=this.props.match.params.id;
        data['round_no']=roundNo;
        axios.post('/api/tournament/round_player_standing/', data).then(response => {
          let roundPlayerStanding = response.data.score
          this.setState({roundPlayerStanding: roundPlayerStanding, loadingModal: false})
        }).catch(error => {
          console.log(error);
        })
    }
    render() {
        let players,standings, roundButtons, modal = null
        if (!this.state.error){
            players = <p>Please wait... :)</p>
            if(this.state.loading){
                players = <Spinner/>
            }
            else if (this.state.players) {
                players = <PlayersList players={this.state.players} />
            }
            if(this.state.loadingModal){
              modal = <Spinner />
            }

            else if(this.state.showSwissPairs){
                modal = (<Aux>
                    <h2>Round no. #{this.state.currentRound}</h2>
                    <Round pairs={this.state.swissPairs}
                    cancel={this.roundCancelHandler} submit={this.loadComponent.bind(this)}
                    currentRound={this.state.currentRound} tournamentId={this.props.match.params.id}/>
                    </Aux>);
            }
            else if(this.state.showStanding){
                modal = (<Aux>
                    <h2>Result of this round</h2>
                    <PlayerStanding scores={this.state.roundPlayerStanding} />
                    </Aux>);
            }
        }
        standings = <PlayerStanding scores={this.state.scores} />
        roundButtons = (<RoundButtons rounds={this.state.rounds} currentRound={this.state.currentRound}
            showResult={this.showResult.bind(this)} tournamentStatus={this.state.tournamentStatus}
            clicked={this.startRoundHandler.bind(this)}/>);
        return(
            <Aux>
                <div className="row">
                    <div className="col-md-4">
                        <ul> Players:
                            {players}
                        </ul>
                        {roundButtons}
                    </div>
                    <div className="col-md-8">
                        <ol> Player Standings:
                            {standings}
                        </ol>
                    </div>
                </div>
                <Modal show={this.state.playing} modalClosed={this.roundCancelHandler}>
                    {modal}
                </Modal>
            </Aux>
        );
    }
}


export default Rounds;

/*
<div>
                  <ul> Players:
                      {players}
                  </ul>
                </div>
                <div>
                  <ol> Player Standings:
                    {standings}
                  </ol>
                </div>*/