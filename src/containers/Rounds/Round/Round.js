import React, {Component} from 'react';
import Aux from '../../../hoc/Aux';
import Button from '../../../components/UI/Button/Button';
import axios from 'axios';

class Round extends Component {
    state = {
        result: {

        }
    }
    winnerSelectorHandler (pair, event, index) {
        let player1 = pair[0];
        let player2 = pair[1];
        let winner = event.target.value;
        let newResult = {...this.state.result}
        newResult[index] = [player1,player2,winner];
        this.setState({ result: newResult })
    }
    onSubmitHandler () {
        let newData = {...this.state.result}
        let data = {player_one: [], player_two: [], winner: []}
        let returned = Object.keys(newData).map(match => {
            data['player_one'].push(newData[match][0])
            data['player_two'].push(newData[match][1])
            data['winner'].push(newData[match][2])
            return true;
        })
        axios.post('/api/tournament/'+this.props.tournamentId+'/status/'+this.props.currentRound+'/data/', data).then(response => {
            this.props.submit()
            this.props.cancel()
        }).catch(error => {
            console.log(error);
        });
    }
    render() {
        const roundSummary = this.props.pairs.map((pair, index) => {
            return (
                <tr key={pair}>
                  <td>
                      {pair[0]}
                  </td>
                  <td>
                      {pair[1]}
                  </td>
                  <td>
                  <select id={pair} onChange={(event) => this.winnerSelectorHandler(pair, event, index)}>
                      <option value= "">Select Winner--</option>
                      <option value={pair[0]}>{pair[0]}</option>
                      <option value={pair[1]}>{pair[1]}</option>
                  </select>
                  </td>
                </tr>);
        });
        return (
            <Aux>
                <h3>Select Winners</h3>
                <table>
                <thead>
                  <tr>
                    <th>Player 1</th>
                    <th>Player 2</th>
                    <th>Winner</th>
                  </tr>
                  </thead>
                  <tbody>
                    {roundSummary}
                  </tbody>
                </table>
                <Button btnType="Danger" clicked={this.props.cancel}>Cancel</Button>
                <Button btnType="Success" clicked={this.onSubmitHandler.bind(this)}>Submit</Button>
            </Aux>
        );
    }
}

export default Round;


/*onchange(numberOfPairs) {
    loop(numberOfPairs){
        getelembyid -> get selected value
        push in an list
        this.setState({state: list})
    }
}

loop {
    dropdowns(id, onchange)
}*/