import React, {Component} from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button'
import Aux from '../../hoc/Aux';
import PlayersList from '../../components/PlayersList/PlayersList'
/*import classes from './Tournaments.css'*/


class Players extends Component {
    state = {
        addPlayerName: '',
        players: null,
        userPlayers: null,
        tournamentStatus: null,
        loading: false,
        error: false,
        input: "",
        dropdown: null,
        update: false
    }
    loadComponent () {
        
        this.setState({loading: true});
        axios.get( '/api/tournament/'+ this.props.match.params.id).then( response => {
                if(response.data.tournament_status > 1){
                    this._history.push('/tournament/'+ this.props.match.params.id+'/rounds')
                }
                const allPlayers = response.data
                this.setState( { players: allPlayers['player'],
                                 userPlayers: allPlayers['user_player'],
                                 tournamentStatus: allPlayers['tournament_status'],
                                 loading: false, error: false } )
            }).catch( error => {
                console.log( error );
                this.setState({loading: false, error: true});
            } );
    }

    componentDidMount () {
        this.loadComponent();
    }
    dropdownChangedHandler(event) {
        this.setState({dropdown: event.target.value})
        let data = {"player": event.target.value}
        axios.post('/api/tournament/'+ this.props.match.params.id+'/', data).then( response => {
            let newPlayerState = {...this.state.players}
            let newUserPlayerState = {...this.state.userPlayers}
            let playerid=response.data.id;
            let playerName=response.data.name;
            newPlayerState[playerid]=[playerName]
            delete newUserPlayerState[playerName]
            this.setState({players: newPlayerState, userPlayers: newUserPlayerState});
            this.loadComponent();
        }).catch(error => {
            console.log(error);
        })
    }
    addPlayerHandler=()=>{
        let data = {"player": this.state.input}
        axios.post('/api/tournament/'+ this.props.match.params.id+'/', data).then( response => {
            let newPlayerState = {...this.state.players}
            let newUserPlayerState = {...this.state.userPlayers}
            let playerid=response.data.id;
            newPlayerState[playerid]=[response.data.name]
            this.setState({players: newPlayerState, input: "", userPlayers: newUserPlayerState});
            this.loadComponent();
        }).catch(error => {
            console.log(error);
        })
    }
    powerOf2 (n) {
        if (typeof n !== 'number') 
            return false; 
        else if(n<2)
            return false;

            return n && (n & (n - 1)) === 0;
    }
    changeHandler =(event)=> {
        this.setState({input: event.target.value})
    }
    startMatchHandler () {
        let players={...this.state.players}
        let numberOfPlayers = Object.keys(players).length;
        const play = (this.powerOf2(numberOfPlayers));
        if(play){
            this.props.history.push(this.props.match.url+'/rounds');
        }
        else{
            alert("Player is not in the form of 2^n");
        }
    }
    render() {
        let tournaments = <p>Sorry something went wrong!</p>
        let dropdown = null
        let players, userPlayers, input = null
        if (!this.state.error){
            players = <p>Please wait... :)</p>
            userPlayers = [{value: "" , displayValue: 'Please wait....'}]
            dropdown = {options: [{value: "" , displayValue: 'Please wait....'}] }
            if(this.state.loading){
                players = <Spinner/>
            }
            else if (this.state.players) {
                players = <PlayersList players={this.state.players} />
            }
            input = {type: 'text', placeholder: 'Enter tournament name' }
        }
        if (this.state.players){
            dropdown = {options: [{value: null , displayValue: 'Select one--'}] }
            for (let i in this.state.userPlayers) {
                dropdown.options.push({ value: this.state.userPlayers[i], displayValue: this.state.userPlayers[i] });
            }
        }
        return(
            <Aux>
                <Input label='Add Player' elementConfig={input} changed={this.changeHandler} value={this.state.input}></Input>
                <Button btnType="Success" clicked={this.addPlayerHandler}> Add </Button>
                <Input elementType="select" elementConfig={dropdown} changed={this.dropdownChangedHandler.bind(this)} />
                <div>
                <ul> Players:
                    {players}
                </ul>
                </div>
                <Button btnType="Success" clicked={this.startMatchHandler.bind(this)}> Start Match! </Button>
            </Aux>
        );
    }
}


export default Players;




/*let rows = [];
                for (let i in this.state.players) {
                    rows.push(
                        <div className="playersList" key={i+'a'}>
                            <li key ={i+'b'} >{this.state.players[i]}</li>
                        </div>
                    );
                }
                players = rows;
            }*/