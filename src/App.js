import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import Signup from './components/Signup/Signup';
import Login from './components/Login/Login';
import Tournaments from './containers/Tournaments/Tournaments';
import Players from './containers/Players/Players';
import Rounds from './containers/Rounds/Rounds';
import Logout from './components/Logout/Logout';
import LoggedIn from './components/LoggedIn/LoggedIn';

class App extends Component {
  state = {
      loggedIn: false,
  };
  componentDidMount () {
      if(window.localStorage.getItem('X-Auth-Token')){
        this.setState({loggedIn: true});
    }
  }
  componentDidUpdate () {

  }

  renderLoginComponent({ history }) {
    this._history = history
    return <Login onLoggedIn={this.onLoggedIn.bind(this)} />
  }

  onLoggedIn() {
    this.setState({loggedIn: true}, () => {
      this._history.push('/tournaments')
    })
  }
  onLoggedOut() {
    this.setState({loggedIn: false}, () => {
      console.log(this);
      this._history.push('/login')
    })
  }

  render() {
    let loggedIn = this.state.loggedIn;
    if(window.localStorage.getItem('X-Auth-Token')){
        loggedIn = true;
    }
    window.app = this
    let route = null;
    let logout=null;
    if (loggedIn){
        route = [
            <Route key={1} path="/signup" exact component={LoggedIn} />,
            <Route key={2} path="/login" exact component={LoggedIn} />,
            <Route key={3} path="/tournaments" exact component={Tournaments} />,
            <Route key={4} path="/tournaments/:id" exact component={Players} />,
            <Route key={5} path="/tournaments/:id/rounds" exact component={Rounds} />,
            <Redirect key={6} from="/" to='/tournaments' />
        ]
        logout=<Logout onLoggedOut={this.onLoggedOut.bind(this)}/>
    }
    else {
        route = [
            <Route key={1} path="/signup" exact component={Signup} />,
            <Route key={2} path="/login" exact render={this.renderLoginComponent.bind(this)} />,
            <Redirect key={3} from="/tournaments" to='/login' />,
            <Redirect key={4} from="/tournaments/:id" to='/login' />,
            <Redirect key={5} from="/tournaments/:id/rounds" to='/login' />,
            <Redirect key={6} from="/" to='/login' />
        ]
        logout=<div></div>
    }
    return (
      <div className="App">
        <header className="App-header">
          {logout}
        </header>
        <div className="container"> 
          <BrowserRouter>
            <Switch>
              {route}
            </Switch>
          </BrowserRouter>
          
        </div>
        <footer>
          <div>
            <p></p>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
