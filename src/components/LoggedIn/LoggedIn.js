import React from 'react';
import {Link} from 'react-router-dom';
import Aux from '../../hoc/Aux';
const loggedIn = () => {
    return (
      <Aux>
        <p>Already logged in!</p>
        <Link to="/tournaments"> <h3>Go to your tournaments</h3></Link>
      </Aux>
    )
}


export default loggedIn;