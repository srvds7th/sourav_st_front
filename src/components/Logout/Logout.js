import React, {Component} from 'react';
import Button from '../UI/Button/Button';



/*const logout = (props) => {
    return (
        <Button clicked={(props) => {
            window.localStorage.removeItem('X-Auth-Token');
            console.log(props)
            props.history.push('/')
          }} btnType="Logout">Logout</Button>
    )
}
*/
class Logout extends Component {
    logoutHandler (){
      window.localStorage.removeItem('X-Auth-Token');
      this.props.onLoggedOut();
      console.log(this.props)
      /*this.props.history.push('/')*/
    }
    render() {
        return (
            <Button clicked={this.logoutHandler.bind(this)} btnType="Logout">Logout</Button>
        );
    }
}

export default Logout;