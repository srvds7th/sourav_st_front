import React from 'react';
import classes from './PlayerStanding.css'


const playerStanding = (props) =>{
    let rows = [];
    for (let i in props.scores) {
        rows.push(
            <tr key={i+'a'}>
                <td key = {i +'b'} >{props.scores[i][0]}</td>
                <td key = {i +'c'} >{props.scores[i][1]}</td>
            </tr>
        );
    }
    return (
      <div className={classes.PlayerStanding}>
      <table>
        <thead>
          <tr>
            <th>Player name</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
      </div>
    );
}


export default playerStanding;