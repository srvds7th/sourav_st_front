import React from 'react';


const playersList = (props) =>{
    let rows = [];
    for (let i in props.players) {
        rows.push(
            <div className="playersList" key={i+'a'}>
                <li key ={i+'b'} >{props.players[i]}</li>
            </div>
        );
    }
    return rows
}


export default playersList;