import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
import Input from '../../components/UI/Input/Input';

class LoginData extends Component {
    state = {
        loginForm: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Username'
                },
                value: ''
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter Password'
                },
                value: ''
            },
        },
        loading: false
    }
    signupHandler = () => (this.props.history.push('/signup'))

    loginHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const formData = {};
        for (let formElementIdentifier in this.state.loginForm) {
            formData[formElementIdentifier] = this.state.loginForm[formElementIdentifier].value;
        }
        axios.post( '/api/login/', formData )
            .then( response => {
                this.setState( { loading: false } );
                window.localStorage.setItem('X-Auth-Token', response.data.token)
                axios.defaults.headers.common['X-Auth-Token']=window.localStorage.getItem('X-Auth-Token');
                this.props.onLoggedIn()
            } )
            .catch( error => {
              console.log("Error is thrown")
                this.setState( { loading: false } );
            } );
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedLoginForm = {
            ...this.state.loginForm
        };
        const updatedFormElement = { 
            ...updatedLoginForm[inputIdentifier]
        };
        updatedFormElement.value = event.target.value;
        updatedLoginForm[inputIdentifier] = updatedFormElement;
        this.setState({loginForm: updatedLoginForm});
    }

    render () {
        window.login = this
        const formElementsArray = [];
        for (let key in this.state.loginForm) {
            formElementsArray.push({
                id: key,
                config: this.state.loginForm[key]
            });
        }
        let form = (
            <form onSubmit={this.loginHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button btnType="Success">Login</Button>
            </form>
        );
        if ( this.state.loading ) {
            form = <Spinner />;
        }
        return (
            <div>
                <h4>Enter your Data</h4>
                {form}
                <span>Don't have an account? </span><Link to={{
                                pathname: '/signup',
                            }}>Signup!</Link>
            </div>
        );
    }
}

export default LoginData;