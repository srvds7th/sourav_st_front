import React, { Component } from 'react';

import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
import Input from '../../components/UI/Input/Input';

class SignupData extends Component {
    state = {
        signupForm: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Username'
                },
                value: ''
            },
            first_name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your First name'
                },
                value: ''
            },
            last_name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Last name'
                },
                value: ''
            },
            password1: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter Password'
                },
                value: ''
            },
            Password2: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Confirm Password'
                },
                value: ''
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-Mail'
                },
                value: ''
            },
            country_code: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: "" , displayValue: 'Select one--'},
                        {value: 100, displayValue: 'USA'},
                        {value: 101, displayValue: 'India'},
                        {value: 102, displayValue: 'England'},
                        {value: 103, displayValue: 'China'},
                        {value: 104, displayValue: 'Switzerland'},
                        {value: 105, displayValue: 'Canada'},
                        {value: 106, displayValue: 'Spain'}
                    ]
                },
                value: ''
            }
        },
        loading: false
    }
    /*componentWillUpdate() {
        console.log(this.state.signupForm);
    }
    componentDidMount() {
        console.log(this.state.signupForm);
    }
    resetHandler = () => {
      console.log("Reset is being handled.....");
        let updatedSignupForm = [
            ...this.state.signupForm
        ];
        console.log(updatedSignupForm);
        let field = {};
        var myobj = JSON.parse(JSON.stringify({
              ...this.state.updatedSignupForm
          }));
        console.log(myobj);
        for (field in updatedSignupForm){
          debugger
            console.log(updatedSignupForm.field);
            /*updatedSignupForm.field.value = '';
        }
        /*updatedSignupForm = updatedSignupForm.map(field => {
            field.value = ''
        });*/
        /*this.setState({signupForm: updatedSignupForm});
        this.componentWillUpdate();
    }*/
    signupHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const formData = {};
        for (let formElementIdentifier in this.state.signupForm) {
            formData[formElementIdentifier] = this.state.signupForm[formElementIdentifier].value;
        }
        axios.post( '/api/signup/', formData )
            .then( response => {
                this.setState( { loading: false } );
                this.props.history.push( '/login' );
            } )
            .catch( error => {
                this.setState( { loading: false } );
            } );
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedSignupForm = {
            ...this.state.signupForm
        };
        const updatedFormElement = { 
            ...updatedSignupForm[inputIdentifier]
        };
        updatedFormElement.value = event.target.value;
        updatedSignupForm[inputIdentifier] = updatedFormElement;
        this.setState({signupForm: updatedSignupForm});
    }

    render () {
        const formElementsArray = [];
        for (let key in this.state.signupForm) {
            formElementsArray.push({
                id: key,
                config: this.state.signupForm[key]
            });
        }
        let form = (
            <form onSubmit={this.signupHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button type="submit" btnType="Success">Submit</Button>
                <Button type="reset" btnType="Danger" clicked={this.resetHandler}>Reset</Button>
            </form>
        );
        if ( this.state.loading ) {
            form = <Spinner />;
        }
        return (
            <div>
                <h4>Enter your Data</h4>
                {form}
            </div>
        );
    }
}

export default SignupData;