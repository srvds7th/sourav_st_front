import React, { Component } from 'react';
import Button from '../UI/Button/Button'

class RoundButtons extends Component {
    render() {
        let rows = [];
        for (let i in this.props.rounds) {
            if(this.props.currentRound === this.props.rounds[i] && this.props.tournamentStatus < 3){
                rows.push(
                    <div key={i+'x'}>
                        <Button key ={i+'y'} btnType="Success" clicked={() => {this.onClick(this.props.rounds[i])}}
                        value={this.props.rounds[i]}>Round {this.props.rounds[i]}</Button>
                    </div>
                );
            }
            else{
                if(this.props.currentRound > this.props.rounds[i] || this.props.tournamentStatus === 3){
                    rows.push(
                        <div key={i+'m'}>
                            <Button key ={i+'n'} btnType="disabled" clicked={() => {this.onClick(this.props.rounds[i])}}
                            value={this.props.rounds[i]} disabled="true" >Round {this.props.rounds[i]}</Button>
                            <Button key ={i+'o'} btnType="Success" clicked={() => {this.clicked(this.props.rounds[i])}}
                            value={this.props.rounds[i]} >View Result</Button>
                        </div>
                    );
                }
                else{
                    rows.push(
                        <div key={i+'x'}>
                            <Button key ={i+'y'} btnType="disabled" clicked={() => {this.onClick(this.props.rounds[i])}}
                            value={this.props.rounds[i]} disabled="true" >Round {this.props.rounds[i]}</Button>
                        </div>
                    );
                }
            }
        }
        return rows
    }

    onClick ( roundNo ) {
        let Round = roundNo
        this.props.clicked(Round)
    }
    clicked ( roundNo ) {
        this.props.showResult(roundNo)
    }
}

/*const roundButtons = (props) =>{
    let rows = [];
    for (let i in props.rounds) {
        if(props.currentRound === props.rounds[i]){
            rows.push(
                <div className="playersList" key={i+'a'}>
                    <Button key ={i+'b'} btnType="Success" clicked={props.clicked} value={props.rounds[i]}>Round {props.rounds[i]}</Button>
                </div>
            );
        }
        else{
            rows.push(
                <div className="playersList" key={i+'a'}>
                    <Button key ={i+'b'} btnType="Success" clicked={props.clicked} value={props.rounds[i]} disabled >Round {props.rounds[i]}</Button>
                </div>
            );
        }
    }
    return rows
}*/


export default RoundButtons;